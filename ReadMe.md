Running Server :

1. Start Xampp and make sure that websocket is active (check on php.ini if the line with extension=php_sockets.dll is uncommented)

2. Change host address in page_mobile.php, page_desktop.php and server.php to your server host IP

3. Go to your shell command-line interface (on Xampp, just click on Shell button)

4. type: 
	php -q f:\work\websocket\www\server.php
	Where c:\path is your path to the files inside htdocs Xampp directory

5. Using browser, navigate to page_mobile.php on your mobile, and page_desktop.php on your desktop



To connect to server and send data:

1) Connect to the server:
   ws://192.168.0.18:9000/demo/server.php
   (don´t forget to change server IP and path)

2) Once connected to the server, send data as follows:

	//when receive data from the sensors, just fire this function to send the data to the server
	
	function sendData() {
		var msg = {
			alpha: alpha,
			beta: beta,
			gamma: gamma,
			acceleration_x: acceleration_x,
			acceleration_y: acceleration_y,
			acceleration_z: acceleration_z,
			ralpha: rotationAcc.alpha,
			rbeta: rotationAcc.beta,
			rgamma: rotationAcc.gamma,
			interval: interval
	   	};
		websocket.send(JSON.stringify(msg));	
	}