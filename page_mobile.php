<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
				<style type="text/css">

				.panel{

					
				margin-right: 3px;
				}

				.button {
				    background-color: #4CAF50;
				    border: none;
				    color: white;
					margin-right: 30%;   
					margin-left: 30%;
				    text-decoration: none;
				    display: block;
				    font-size: 16px;
				    cursor: pointer;
					width:30%;
				    height:40px;
					margin-top: 5px;
					 
				}
				input[type=text]{
						width:100%;
						margin-top:5px;
						
					}


				.chat_wrapper {
					width: 70%;
					height:472px;
					margin-right: auto;
					margin-left: auto;
					background: #3B5998;
					border: 1px solid #999999;
					padding: 10px;
					font: 14px 'lucida grande',tahoma,verdana,arial,sans-serif;
				}
				.chat_wrapper .message_box {
					background: #F7F7F7;
					height:auto;
					overflow: auto;
					padding: 10px 10px 20px 10px;
					border: 1px solid #999999;
				}
				.chat_wrapper  input{
					//padding: 2px 2px 2px 5px;
				}
				.system_msg{color: #BDBDBD;font-style: italic;}
				.user_name{font-weight:bold;}
				.user_message{color: #88B6E0;}

				@media only screen and (max-width: 720px) {
				    /* For mobile phones: */
				    .chat_wrapper {
				        width: 95%;
					height: 40%;
					}
				    

					.button{ width:100%;
					margin-right:auto;   
					margin-left:auto;
					height:40px;}
							
				}

				</style>
		</head>


<body>	


<script src="js/jquery-3.1.1.js"></script>


<script language="javascript" type="text/javascript">  

	var alpha = 0;
	var beta = 0;
	var gamma = 0;

	var acceleration_x = 0;
	var acceleration_y = 0;
	var acceleration_z = 0;

	var rotationAcc = new Object();
	var interval = 0;




	$(document).ready(function(){
		var wsUri = "ws://192.168.0.18:9000/demo/server.php"; 	
		websocket = new WebSocket(wsUri); 
		
		websocket.onopen = function(ev) {

			$('#message_box').append("<div class=\"system_msg\">Connected! Sending data to server.</div>"); 
		
		}


	websocket.onerror	= function(ev){$('#message_box').append("<div class=\"system_error\">Error Occurred - "+ev.data+"</div>");}; 
	websocket.onclose 	= function(ev){$('#message_box').append("<div class=\"system_msg\">Connection Closed</div>");}; 



	window.addEventListener("deviceorientation", function(event) {
          alpha = event.alpha;
          beta = event.beta;
          gamma = event.gamma;
          var html = 'Device Orientation:<br />';
	      html += 'Alpha: ' + alpha +'<br />Beta: ' + beta + '<br/>Gamma: ' + gamma+ '<br />';
          $("#message_box1").html(html); 
          sendData();   
    }, true);

    
	if(window.DeviceMotionEvent) {
      	window.addEventListener('devicemotion', function(event) {
	        acceleration_x = event.accelerationIncludingGravity.x;
	        acceleration_y = event.accelerationIncludingGravity.y;
	        acceleration_z = event.accelerationIncludingGravity.z;
	        rotationAcc = event.rotationRate;
	        var html2 = 'Device Acceleration:<br />';
	        html2 += 'x: ' + acceleration_x +'<br />y: ' + acceleration_y + '<br/>z: ' + acceleration_z+ '<br />';
	        html2 += 'Device Rotation Rate:<br />';
	        if(rotationAcc!=null) html2 += 'alpha: ' + rotationAcc.alpha +'<br />beta: ' + rotationAcc.beta + '<br/>gamma: ' + rotationAcc.gamma + '<br />';
	        $("#message_box2").html(html2);   
	        sendData();            
      	});
    }

    function getIPInfo(ws) {
	    return {
	        remoteIP: ws.upgradeReq.connection.remoteAddress,
	        family: ws._socket._peername.family,
	        port: ws._socket._peername.port
	    };
	};

	function sendData() {
		var msg = {
			alpha: alpha,
			beta: beta,
			gamma: gamma,
			acceleration_x: acceleration_x,
			acceleration_y: acceleration_y,
			acceleration_z: acceleration_z,
			ralpha: rotationAcc.alpha,
			rbeta: rotationAcc.beta,
			rgamma: rotationAcc.gamma,
			interval: interval
	   	};
		websocket.send(JSON.stringify(msg));	
	}
	  




});


</script>
<div class="chat_wrapper">
<div class="message_box" id="message_box1"></div>
<div class="message_box" id="message_box2"></div>
<div class="panel">

</div>



</div>

</body>
</html>